extends VBoxContainer

var games = {}
onready var checkBox = preload("res://CheckBox.tscn")

func _ready():
	var list = global.load_minigames_from_file()
	for game in list:
		var box = checkBox.instance()
		box.pressed = true
		box.text = game
		games[game] = box
		self.add_child(box)

func _exit_tree():
	var result = []
	for key in games.keys():
		if games[key].pressed:
			result.append(key)
	global.set_current_list_of_games(result)


func _on_Button_pressed():
	var any_pressed = false
	for key in games.keys():
		if games[key].pressed:
			any_pressed = true
			break
	for key in games.keys():
		games[key].pressed = !any_pressed
