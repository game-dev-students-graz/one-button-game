extends Button

export (String) var scene_to_load_path
# export (PackedScene) var scene_to_load

func _ready():
	self.connect("pressed", self, "_on_Button_pressed")

func _on_Button_pressed():
	get_tree().change_scene(scene_to_load_path)

func _unhandled_input(event):
	if event.is_pressed():
		_on_Button_pressed()
