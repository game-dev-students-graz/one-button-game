extends Control

onready var getReadyContainer = $Left/GetReadyContainer
onready var scoreLabel = $Left/ScoreLabel
onready var livesLabel = $Right/LivesLabel
onready var tween = $Tween
onready var successLabel = $Left/SuccessLabel
onready var failLabel = $Right/FailLabel

enum TransitionState {INIT, FAIL, WIN}

func start_transition(runde: int, lives: int, state: int):
	visible = true
	if state == TransitionState.INIT:
		successLabel.visible = false
		failLabel.visible = false
		getReadyContainer.visible = true
		scoreLabel.text = ""
	elif state == TransitionState.WIN:
		successLabel.visible = true
		failLabel.visible = false
	elif state == TransitionState.FAIL:
		successLabel.visible = false
		failLabel.visible = true
	tween.interpolate_callback(self, 0.4, "update_score_label", runde)
	livesLabel.text = str(lives)
	var leftPosition = Vector2(-640, 0)
	var rightPosiiton = Vector2(640, 0)
	var outerRightPosition = Vector2(640 * 2, 0)
	tween.interpolate_property($Left, "rect_position", leftPosition, Vector2.ZERO, 0.2, Tween.TRANS_QUAD, Tween.EASE_IN)
	tween.interpolate_property($Right, "rect_position", outerRightPosition, rightPosiiton, 0.2, Tween.TRANS_QUAD, Tween.EASE_IN)
	tween.start()

func end_transition():
	# visible = false
	getReadyContainer.visible = false
	var leftPosition = Vector2(-640, 0)
	var rightPosiiton = Vector2(640, 0)
	var outerRightPosition = Vector2(640 * 2, 0)
	tween.interpolate_property($Left, "rect_position", Vector2.ZERO, leftPosition, 0.2, Tween.TRANS_QUAD, Tween.EASE_IN)
	tween.interpolate_property($Right, "rect_position", rightPosiiton, outerRightPosition, 0.2, Tween.TRANS_QUAD, Tween.EASE_IN)
	tween.start()

func update_score_label(runde):
	scoreLabel.text = "Round " + str(runde)
	
func hide_text():
	scoreLabel.visible = false
	successLabel.visible = false
	failLabel.visible = false
	livesLabel.visible = false
	$Right/LivesIcon.visible = false
		
func show_text():
	scoreLabel.visible = true
	livesLabel.visible = true
	$Right/LivesIcon.visible = true
