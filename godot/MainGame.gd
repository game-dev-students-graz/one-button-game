extends Node2D

var list_of_games : Array
var current_scene
var current_game_won

var runde
var lives
export (int) var start_lives = 3
var game_timer = 0.0
var game_duration

export (float) var difficulty_increase_per_game = 3
export (float) var difficulty_decrease_per_loss = 0
export (float) var starting_difficulty = 1
var max_difficulty = 100
var current_difficulty = starting_difficulty

export (int) var maximum_highscore_entries = 5
var lastHighScorePosition = -1

onready var WinScreen = preload("res://WinScreen.tscn")
onready var LoseScreen = preload("res://LoseScreen.tscn")
onready var GameOver = preload("res://GameOver.tscn")

var game_over_scene

enum GameState {INIT, GAME_TRANSITION, GAME_INTRO, GAME_RUNNING, GAME_DONE, GAME_OVER, HIGH_SCORE_ENTRY, VIEW_HIGH_SCORE}
var status : int = GameState.INIT

enum TransitionState {INIT, FAIL, WIN}

# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	game_over_scene = GameOver.instance()
	$BetweenTimer.connect("timeout", self, "on_sceneswitch_timer")
	$IntroTimer.connect("timeout", self, "on_intro_timeout")
	$OutroTimer.connect("timeout", self, "on_outro_timeout")
	list_of_games = global.get_current_list_of_games()
	reset()

func reset():
	runde = 1
	lives = start_lives
	current_difficulty = starting_difficulty
	$HiScoreView.visible = false
	status = GameState.INIT
	$MainUI.start_transition(runde, lives, TransitionState.INIT)
	$TimeProgressBar.visible = false
	$BetweenTimer.start()

func show_high_scores():
	status = GameState.VIEW_HIGH_SCORE
	$HiScoreView/HiScores.bbcode_text = global.top_scores_formatted(maximum_highscore_entries, lastHighScorePosition)
	$HiScoreView.visible = true

func _process(delta):
	if status == GameState.GAME_RUNNING:
		game_timer += delta
		$TimeProgressBar.value = game_timer
		if game_timer >= game_duration:
			current_scene.on_game_timeout()
	

func _input(event):
	if event is InputEventKey:
		if event.scancode == KEY_ESCAPE and event.is_pressed():
			get_tree().quit()
	if status == GameState.GAME_OVER:
		if event.is_action_pressed("game_big_button_00"):
			remove_child(game_over_scene)
			lastHighScorePosition = global.position_of_score(runde)
			if global.position_of_score(runde) < maximum_highscore_entries:
				status = GameState.HIGH_SCORE_ENTRY
				$HiScore.visible = true
				$HiScore.reset(runde)
			else:
				show_high_scores()
	elif status == GameState.HIGH_SCORE_ENTRY:
		if event.is_action_pressed("game_big_button_00"):
			if $HiScore.is_done():
				var name = $HiScore.get_entered_name()
				global.add_high_score(name, runde)
				$HiScore.visible = false
				show_high_scores()
	elif status == GameState.VIEW_HIGH_SCORE:
		if event.is_action_pressed("game_big_button_00"):
			$MainUI.show_text()
			reset()

var lastFewGames = []
export (int) var minGamesBeforeRepeat = 3

func determineNextGameToLoad():
	var notRecentlyPlayed = list_of_games.duplicate()
	for old_game in lastFewGames:
		notRecentlyPlayed.erase(old_game)
	var gameToLoad
	if notRecentlyPlayed.size() > 0:
		gameToLoad = notRecentlyPlayed[randi() % len(notRecentlyPlayed)]
	else:
		lastFewGames.clear()
		gameToLoad = list_of_games[randi() % len(list_of_games)]
	lastFewGames.append(gameToLoad)
	if lastFewGames.size() > minGamesBeforeRepeat:
		lastFewGames.remove(0)
	return gameToLoad

func loadRandomGame():
	var gameToLoad = determineNextGameToLoad()
	var gameScene = load(gameToLoad)
	var gameSceneInstance = gameScene.instance()
	
	current_scene = gameSceneInstance
	gameSceneInstance.connect("game_finished", self, "on_game_finished_deferred", [], CONNECT_DEFERRED)
	gameSceneInstance.connect("setup_timer", self, "on_setup_timer")
	$IntroTimer.connect("timeout", gameSceneInstance, "on_intro_over", [], CONNECT_ONESHOT)
	status = GameState.GAME_INTRO
	gameSceneInstance.init_game(current_difficulty)
	$MinigameContainer.add_child(gameSceneInstance)
	
	$Tween.interpolate_property($TransitionRect, "modulate", $TransitionRect.modulate, Color(1,1,1,0), 0.2, Tween.TRANS_CIRC, Tween.EASE_OUT)
	$Tween.start()

func unloadGame():
	if current_scene != null:
		current_scene.free()
		current_scene = null

func on_game_finished_deferred(did_win, outro_duration = 0):
	if status == GameState.GAME_RUNNING:
		status = GameState.GAME_DONE
		current_game_won = did_win
		if outro_duration > 0:
			$OutroTimer.start(outro_duration)
		else:
			on_game_outro_finished()

func on_game_outro_finished():
	if status == GameState.GAME_DONE:
		status = GameState.GAME_TRANSITION
		var transitionState
		if current_game_won:
			change_difficulty(difficulty_increase_per_game)
			transitionState = TransitionState.WIN
			$Tween.interpolate_property($TransitionRect, "modulate", $TransitionRect.modulate, Color(0,1,0,1), 0.2, Tween.TRANS_CIRC, Tween.EASE_IN)
		else:
			lives -= 1
			change_difficulty(-difficulty_decrease_per_loss)
			
			transitionState = TransitionState.FAIL
			$Tween.interpolate_property($TransitionRect, "modulate", $TransitionRect.modulate, Color(1,0,0,1), 0.2, Tween.TRANS_CIRC, Tween.EASE_IN)
		if lives > 0:
			runde += 1
		$MainUI.start_transition(runde, lives, transitionState)
		$TimeProgressBar.visible = false
		$Tween.start()
		$BetweenTimer.start()

func change_difficulty(amount):
	current_difficulty = clamp(current_difficulty + amount, starting_difficulty, max_difficulty)

func on_sceneswitch_timer():
	switch_scene()

func switch_scene():
	unloadGame()
	if lives <= 0:
		add_child(game_over_scene)
		$MainUI.hide_text()
		status = GameState.GAME_OVER
	else:
		$MainUI.end_transition()
		loadRandomGame()

func on_setup_timer(duration, intro_duration):
	game_timer = 0
	game_duration = duration
	$TimeProgressBar.value = 0
	$TimeProgressBar.max_value = duration
	if intro_duration > 0:
		$IntroTimer.start(intro_duration)
	else:
		current_scene.on_intro_over()
		on_intro_timeout()

func on_intro_timeout():
	if status == GameState.GAME_INTRO:
		status = GameState.GAME_RUNNING
		$TimeProgressBar.visible = true

func on_outro_timeout():
	on_game_outro_finished()


