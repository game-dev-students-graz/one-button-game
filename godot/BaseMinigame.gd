extends Node

export var minigame_name : String = "default"
export var timer_duration : int = 5
export var intro_duration : float = 0.5
export var outro_duration : float = 0
export var timeout_win : bool = false

var intro_over
export var game_difficulty = 1

signal game_finished
signal setup_timer

func _ready():
	intro_over = false
	emit_signal("setup_timer", self.timer_duration, self.intro_duration)

func on_intro_over():
	intro_over = true

func init_game(difficulty):
	self.game_difficulty = difficulty

func on_game_timeout():
	emit_signal("game_finished", timeout_win, outro_duration)

func scaled_game_difficulty(low, high):
	return low + (high - low) * ((game_difficulty - 1.0) / (100.0 - 1.0))
