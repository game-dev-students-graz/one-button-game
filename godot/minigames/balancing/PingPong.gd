extends "res://BaseMinigame.gd"

var lastSwing = -9999
var swingDuration = 200 # milliseconds
var rechargeDuration = 500
var lastRelease = -9999

var lastRevenge = -9999
var revengeDuration = 200
var lastBoing = -9999
var boingDuration = 100
var lastHit = -9999
var hitDuration = 200

var swinging = false
var down = false

var anchorpoint : Vector2

var ball_velocity : Vector2 = Vector2()
var impact_height = 9000
var gravity = 6000 # pixels/s²

var ball_in_hit_zone
var starting_speed = 0.8
var ball_duration = starting_speed
var min_ball_duration = 0.22
var ball_duration_decrease_absolute = 0.01
var ball_duration_decrease_relative = 0.05

var hits_required = 3
var hits_done = 0

func _ready():
	anchorpoint = $"Schläger".position
	$"Schläger2".visible = false
	$Swing.visible = false
	$Hit.visible = false
	$Boing.visible = false
	$Boing2.visible = false
	starting_speed = scaled_game_difficulty(0.8, 0.6)
	ball_duration_decrease_absolute = scaled_game_difficulty(0.04, 0.01)
	ball_duration_decrease_relative = scaled_game_difficulty(0.05, 0.2)
	hits_required = scaled_game_difficulty(3, 4)

func opponent_hit_ball(duration):
	var x_dist : float = 1000.0
	var y_dist : float = 500.0 - duration * duration * 500
	var pos = $ImpactArea/CollisionShape2D.global_position
	var ext = $ImpactArea/CollisionShape2D.shape.extents
	var impact_pos = pos + Vector2(rand_range(0, ext.x), rand_range(0, ext.y))
	var h_0 = impact_pos.y - gravity * duration * duration * 0.5
	var v_0 = (y_dist - gravity * duration * duration * 0.5) / float(duration)
	var start_pos = Vector2(impact_pos.x - x_dist, impact_pos.y - y_dist)
	$Ball.position = start_pos
	ball_velocity = Vector2(x_dist/duration, v_0)
	impact_height = impact_pos.y
	lastRevenge = OS.get_ticks_msec()
	$Boing2.position.y = start_pos.y + v_0 / 10 + 100

func update_sprites():
	$"Schläger".position = anchorpoint + Vector2(0, 260) * ( 1 - charge())
	$"Schläger".visible = !down
	$"Schläger2".visible = down
	$Swing.visible = swinging
	$Hit.visible = OS.get_ticks_msec() - lastHit < hitDuration
	$Boing.visible = OS.get_ticks_msec() - lastBoing < boingDuration
	$Boing2.visible = OS.get_ticks_msec() - lastRevenge < revengeDuration

func charge():
	var delta = OS.get_ticks_msec() - lastRelease
	return min(1, float(delta) / rechargeDuration)

func _unhandled_input(event):
	if event.is_action_pressed("game_big_button_00"):
		lastSwing = OS.get_ticks_msec()
		$Swing.scale.y = charge()
		swinging = true
		down = true
		if ball_in_hit_zone and ball_velocity.x > 0:
			ball_velocity.x *= -1.2
			lastHit = OS.get_ticks_msec()
			$Hit.position = $Ball.position
	elif event.is_action_released("game_big_button_00"):
		lastRelease = OS.get_ticks_msec()
		down = false

var bla = false

func _process(delta):
	if intro_over and !bla:
		bla = true
		opponent_hit_ball(ball_duration)
	$Ball.position += ball_velocity * delta
	ball_velocity.y += gravity * delta
	if $Ball.position.y > impact_height and ball_velocity.y > 0:
		ball_velocity.y *= -0.85
		lastBoing = OS.get_ticks_msec()
		$Boing.position = $Ball.position
	if $Ball.position.x > 1900:
		ball_duration = starting_speed
		emit_signal("game_finished", false)
	if $Ball.position.x < -500:
		ball_duration -= ball_duration_decrease_absolute
		ball_duration *= (1 - ball_duration_decrease_relative)
		if ball_duration < min_ball_duration:
			ball_duration = min_ball_duration
		hits_done += 1
		if (hits_done >= hits_required):
			emit_signal("game_finished", true)
		else:
			opponent_hit_ball(ball_duration)
	if OS.get_ticks_msec() - lastSwing >= swingDuration:
		swinging = false
	update_sprites()

func _on_Area2D_area_entered(area):
	ball_in_hit_zone = true

func _on_Area2D_area_exited(area):
	ball_in_hit_zone = false


