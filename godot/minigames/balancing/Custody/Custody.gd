extends "res://BaseMinigame.gd"

onready var player = $player
onready var doorLeft = $doorLeft
onready var doorRight = $doorRight

var playerSpeed
var speedup = 1.1

var minPos = 120
var maxPos = 1160

var doorClosedPosY = 370

# Called when the node enters the scene tree for the first time.
func _ready():
	# This is a lazy way to increase difficulty:
	playerSpeed = scaled_game_difficulty(500, 1100)
	speedup = scaled_game_difficulty(0, 100)
	player.position.x = 1280/2
	
	doorLeft.position.y = doorClosedPosY
	doorRight.position.y = 0

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if intro_over:
		player.position.x += playerSpeed * delta
	else:
		player.position.x += playerSpeed * delta * 0.05
	if player.position.x < minPos or player.position.x > maxPos:
		emit_signal("game_finished", false)

func _input(event):
	var pressAllowed = player.position.x >= minPos and player.position.x <= maxPos
	
	if event.is_action_pressed("game_big_button_00") and pressAllowed:
		if intro_over:
			playerSpeed *= -1
			if playerSpeed > 0:
				playerSpeed += speedup
			else:
				playerSpeed -= speedup
		else:
			playerSpeed *= -1
		
		
		var doorLeftOpen = playerSpeed < 0
		player.set_flip_h(doorLeftOpen)
		
		doorLeft.position.y = 0 if doorLeftOpen else doorClosedPosY
		doorRight.position.y = 0 if !doorLeftOpen else doorClosedPosY
