extends "res://BaseMinigame.gd"

onready var player = $player

var playerStartSpeed = 500
var playerSpeed = playerStartSpeed
var speedup = 1.2

var minPos = 0
var maxPos = 1280

# Called when the node enters the scene tree for the first time.
func _ready():
	# This is a lazy way to increase difficulty:
	playerStartSpeed = scaled_game_difficulty(270, 270)
	speedup = scaled_game_difficulty(1, 1)
	player.position.x = 1280/2

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if intro_over:
		player.position.x += playerSpeed * delta
	else:
		player.position.x += playerSpeed * delta * 0.05
	if player.position.x < minPos or player.position.x > maxPos:
		emit_signal("game_finished", false)

func _input(event):
	if event.is_action_pressed("game_big_button_00"):
		if intro_over:
			playerSpeed *= -1
			if playerSpeed > 0:
				playerSpeed += speedup
			else:
				playerSpeed -= speedup
		else:
			playerSpeed *= -1
