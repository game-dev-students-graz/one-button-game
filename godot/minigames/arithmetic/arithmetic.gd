extends "res://BaseMinigame.gd"

var result = 0
var maxModifier = 4;

func _enter_tree():
	
	var a = randi() % int(scaled_game_difficulty(8, 20))
	var b = randi() % int(scaled_game_difficulty(8, 20))
	
	var resultIsTrue = randi() % 2;
	# That is super ugly, but it gets the job done without many ifs
	# If the result is not true, this will add or substract a number between 1 and maxModifier
	var modifier = int(!resultIsTrue) * (1 + randi() % maxModifier) * [1, -1][randi() % 2];
	
	var calculationType = randi() % 2
	if calculationType == 0:
		result = a + b + modifier; 
		$VBoxContainer/ArithmeticLabel.text =  str(a) + " + " + str(b) +  " = " + str(result)
	else:
		result = a - b + modifier;
		$VBoxContainer/ArithmeticLabel.text =  str(a) + " - " + str(b) +  " = " + str(result)
		
		
	timeout_win = !resultIsTrue

# Called when the node enters the scene tree for the first time.
func _input(event):
	if event.is_action_pressed("game_big_button_00"):
		if timeout_win:
			emit_signal("game_finished", false)
		else:
			emit_signal("game_finished", true)
