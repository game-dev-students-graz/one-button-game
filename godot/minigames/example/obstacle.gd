extends Sprite

var movespeed = -1000
var speedMin = -500
var speedMax = -1200
var wrapMin = 0
var wrapMax = 1280

export (NodePath) var rootOfThisExampleMinigame

func _ready():
	position.x = wrapMax + 40
	reset(true)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if get_node(rootOfThisExampleMinigame).intro_over:
		position.x += movespeed * delta
		if position.x < wrapMin:
			position.x += (wrapMax - wrapMin)
			reset()

func reset(intro_speed = false):
	var ratio
	if intro_speed:
		ratio = 0
	else:
		ratio = rand_range(0, 1)
	modulate = Color(ratio, 1 -ratio, 1-ratio)
	movespeed = speedMin + (speedMax - speedMin) * ratio
