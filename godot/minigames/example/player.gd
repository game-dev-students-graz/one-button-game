extends Sprite

var jumpForce = 400
var slamForce = 500
var gravity = 600
var speedy = 0
var floory = position.y
var jumping = false

signal player_dead

# Called when the node enters the scene tree for the first time.
func _process(delta):
	if(jumping):
		self.position.y += speedy * delta
		speedy += gravity * delta
		if(position.y > floory):
			jumping = false
			position.y = floory
			speedy = 0

func _input(event):
	if event.is_action_pressed("game_big_button_00"):
		if not jumping:
			jumping = true
			speedy = -jumpForce
		else:
			speedy = slamForce

func _on_Area2D_area_entered(area):
	emit_signal("player_dead")
