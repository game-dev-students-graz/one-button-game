extends Area2D

# rotation speed
export (float) var rot_speed = 3
export (int) var shoot_speed = 800

var velocity = Vector2()
var init_pos = Vector2()

# aim for something to shoot
var aim = 1
var aim_dir = -1
	
# for hitting the target
var hit_target = false

# rotation direction
var rot_dir = 1
var game_done = false

func _ready():
	
	# initial pos
	init_pos = position

	#rotation = 32.0
	rotation_degrees = -32.0

	# change rot direction
	if randf() < 0.5:
		rot_dir = -1


func _input(event):

	velocity = Vector2()

	# press the button
	if event.is_pressed() and $"..".intro_over:

		# aim is off, you shooted
		aim = 0


# update
func _process(delta):
	
	# begin to rotate
	if $"..".intro_over:
	
		# update rotation
		rotation += rot_dir * rot_speed * delta * aim
	
	# shoot
	if not aim:

		# give it a speed
		velocity = Vector2(0, aim_dir * shoot_speed).rotated(rotation)
	
	# shoot if not hit
	if not hit_target:
		position += velocity * delta
	

func _on_arrow_area_entered(area):
	hit_target = true
	# win
	if not game_done:
		$"..".emit_signal("game_finished", true)
		game_done = true


func _on_arrow_body_entered(body):
	hit_target = true
	# win
	if not game_done:
		$"..".emit_signal("game_finished", true)
		game_done = true


func _on_VisibilityNotifier2D_screen_exited():
	# lose
	if not game_done:
		$"..".emit_signal("game_finished", false)
		game_done = true
