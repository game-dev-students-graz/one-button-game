extends "res://BaseMinigame.gd"

export (PackedScene) var Target

func _ready():
	
	# scale the arrow to difficulty
	var sd = scaled_game_difficulty(0, 0.3)
	$Arrow.scale -= Vector2(sd, sd)
	
	# Choose a random location on Path2D.
	$TargetPath/SpawnLocation.set_offset(randi())
	
	# Create a Mob instance and add it to the scene.
	var target = Target.instance()
	add_child(target)
	
	target.position = $TargetPath/SpawnLocation.position

