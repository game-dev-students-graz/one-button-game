extends "res://BaseMinigame.gd"

var press = true;
var press_button_texts = {"easy" : ["Press the Button!"], 
						  "medium" : ["Press the Button to win!", "Don't Press the Button to lose!"],
						  "hard" : ["Don't press the Button to not win!", "Press the Button to not lose!"]}
var dont_press_button_texts = {"easy" : ["Don't press the Button!"], 
							   "medium" : ["Press the Button to lose!", "Don't Press the Button to win!"],
							   "hard" : ["Don't press the Button to not lose!", "Press the Button to not win!"]}

export var medium_threshold = 30
export var hard_threshold = 70

func _enter_tree():
	self.connect("game_finished", self, "hide_label")
	intro_duration = 0
	var type = randi() % 2
	var difficulty = "easy"
	if game_difficulty > medium_threshold: 
		difficulty = "medium"
	if game_difficulty > hard_threshold:
		difficulty = "hard"
		
	if type == 0:
		press = false
		timeout_win = true
		$"Label".text = dont_press_button_texts[difficulty][randi() % dont_press_button_texts[difficulty].size()]
		
	else:
		press = true
		timeout_win = false
		$"Label".text = press_button_texts[difficulty][randi() % press_button_texts[difficulty].size()]

# Called when the node enters the scene tree for the first time.
func _input(event):
	if event.is_action_pressed("game_big_button_00"):
		$Hand.translation.y = .35
		$Button.visible = false
		$Button_Pressed.visible = true
		$Label.visible = false
		if press:
			emit_signal("game_finished", true)
		else:
			emit_signal("game_finished", false)


func hide_label(win, timer):
	$Label.visible = false
	
	
	
