extends "res://BaseMinigame.gd"

# ######################################################################################################################
# Members

# Settings
onready var count_target = int(ceil(scaled_game_difficulty(3, 25))) + randi() % 3
var platform_distance    = 2
var fly_vertical_move    = 0.5
export var soundVolume   = -12

# States
onready var count_current = 0
onready var zapped        = false
onready var swatted       = false
onready var input_active  = false

# ######################################################################################################################
# Methods

# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# Init

func _ready():
	self.connect("game_finished", self, "game_finish_actions")
	
	# Set SFX Volumes
	$AudioSfxBuzz.volume_db = soundVolume
	$AudioSfxZapp.volume_db = soundVolume
	$AudioSfxSlurp.volume_db = soundVolume
	$AudioSfxSplat.volume_db = soundVolume
	
	# Init labels
	$LabelTarget.text = "Feast ham at plate #" + str(self.count_target)
	$LabelCurrent.text = str(self.count_current)
	
	# Init normal plate
	for i in range(count_target):
		var new_node = $Plate.duplicate()
		new_node.translate(Vector3(platform_distance*i, 0, 0))
		get_node('.').add_child(new_node)
		
	# Init ham plate
	var new_node = $Plate.duplicate()
	new_node.translate(Vector3(platform_distance*count_target, 0, 0))
	get_node('.').add_child(new_node)
	$Ham.translation.x = platform_distance*count_target
		
	# Init zapper
	$Zapper.translate(Vector3(platform_distance*(count_target + 1), 0, 0))
	
	# Activate input
	input_active = true
	
	if Input.is_action_pressed("game_big_button_00"):
		$Fly.translation.y -= fly_vertical_move

func game_finish_actions(did_win, outro_duration = 0):
	self.input_active = false
	
	if not Input.is_action_pressed("game_big_button_00"):
		$Fly.translation.y -= fly_vertical_move
	
	if timeout_win:
		$AudioSfxSlurp.play()
		
	elif !zapped:
		$AudioSfxSplat.play()
		swatted = true
	
	$LabelTarget.visible = false
	$LabelCurrent.visible = false
# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# Input

func _input(event):
	
	if !input_active:
		return
		
	if event.is_action_pressed("game_big_button_00") && (count_current <= count_target):
		
		# Fly down
		$Fly.translation.y -= fly_vertical_move
		
	
	# If button released
	if event.is_action_released("game_big_button_00"):
		
		# Fly up
		$Fly.translation.y += fly_vertical_move
		
		# Increment counter and update label
		self.count_current += 1
		$LabelCurrent.text = str(self.count_current)
		
		# Check win
		timeout_win = count_current == count_target

		# Count up and effects
		if(count_current <= count_target):
			# Play sound
			$AudioSfxBuzz.play(1.95)
		
		# If fly went too far
		if(count_current == count_target + 1 && !zapped):
			$ScreenSplash.visible = true
			zapped = true
			input_active = false
			emit_signal("game_finished", false, 0.6)
			$AudioSfxZapp.play()
			

# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# Movement

func _process(delta):
	
	var current_focus = count_current * platform_distance
	
	if($Camera.translation.x < current_focus):
		var move_update = (current_focus - $Camera.translation.x) * delta * 3.5
		$Camera.translation.x += move_update
		
	if($Fly.translation.x < current_focus):
		var move_update = (current_focus - $Camera.translation.x) * delta * 4
		$Fly.translate(Vector3(move_update, 0, 0))
		
	$Swatter.translation.x = $Fly.translation.x
	
	if(zapped):
		$Fly.translation.y -= 2 * delta
		
	if(swatted && $Swatter.translation.y > 0.1):
		$Swatter.translation.y -= 10 * delta
		
	if(swatted && $Fly.translation.y > -0.2):
		$Fly.translation.y -= 5 * delta
