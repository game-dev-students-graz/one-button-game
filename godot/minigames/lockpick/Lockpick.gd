extends "res://BaseMinigame.gd"

onready var lockScene = preload("bolt.tscn")
onready var firstLock = $boltContainer/bolt
onready var player = $player

var locks = []
var loose_locks = []

var min_x
var max_x
var floor_y
var max_y
var margin = 20
var initialSpeed = 400
var playerspeed
var number_of_loose_locks

var bolt_offset = 66

enum State {INIT, AIMING, PICKING, RECOIL}

var status = State.INIT

func _enter_tree():
	number_of_loose_locks = scaled_game_difficulty(1, 3)
	timer_duration = 4 + number_of_loose_locks * 0.1

func _ready():
	floor_y = player.position.y
	player.get_child(0).connect("area_entered", self, "hit_area") 
	locks.append(firstLock)
	max_y = firstLock.position.y + 64 - 20
	min_x = firstLock.position.x - margin
	for i in range(5):
		var lock = lockScene.instance()
		lock.position = firstLock.position + Vector2(121.5, 0) * (i + 1)
		locks.append(lock)
		$boltContainer.add_child(lock)
		max_x = lock.position.x + margin
	var free_locks = locks.duplicate()
	for i in range(number_of_loose_locks):
		var unlock_index = randi() % free_locks.size()
		loose_locks.append(free_locks[unlock_index])
		free_locks.remove(unlock_index)
	
	for lock in loose_locks:
		# lock.modulate = Color(1, 0, 1)
		lock.position.y += bolt_offset
	playerspeed = initialSpeed

func hit_area(area : Area2D):
	var par = area.get_parent()
	if loose_locks.has(par):
		# par.modulate = Color(0, 1, 1)
		var duration = 0.1
		$Tween.interpolate_property(par, "position", par.position, par.position + Vector2(0, -bolt_offset), duration, Tween.TRANS_CUBIC, Tween.EASE_OUT)
		$Tween.interpolate_callback(self, duration, "in_place")
		loose_locks.erase(par)
		$Tween.start()
	else:
		status = State.RECOIL

func in_place():
	if loose_locks.size() <= 0:
		emit_signal("game_finished", true)

func _process(delta):
	if status == State.INIT:
		if intro_over:
			status = State.AIMING
	elif status == State.AIMING:
		player.position.x += playerspeed * delta * (1 + game_difficulty * 0.02)
		if (playerspeed < 0 and player.position.x < min_x) or (playerspeed > 0 and player.position.x > max_x):
			playerspeed *= -1
	elif status == State.PICKING:
		player.position.y -= initialSpeed * delta
		if player.position.y < max_y:
			status = State.RECOIL
	elif status == State.RECOIL:
		player.position.y += initialSpeed * 0.5 * delta
		if player.position.y > floor_y:
			status = State.AIMING
			player.position.y = floor_y

func _input(event):
	if event.is_action_pressed("game_big_button_00"):
		if status == State.AIMING:
			status = State.PICKING
