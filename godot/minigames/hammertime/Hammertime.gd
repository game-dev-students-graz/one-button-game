extends "res://BaseMinigame.gd"

onready var lockScene = preload("nail.tscn")
onready var firstLock = $nailscontainer/nail
onready var player = $hammer

var locks = []
var loose_locks = []

var min_x
var max_x
var floor_y
var max_y
var margin = 45
var initialSpeed = 600
var secondRow
var playerspeed
var number_of_loose_locks
var timer_of_hammering
var hammer_duration = 0.1
var stun_duration = 0.5

enum State {INIT, AIMING, HAMMERING, DONE}

var status = State.INIT

func _enter_tree():
	number_of_loose_locks = scaled_game_difficulty(2, 12)
	timer_duration = 5 - scaled_game_difficulty(0, 1)

func _ready():
	floor_y = player.position.y
	secondRow = floor_y + 100
	locks.append(firstLock)
	max_y = firstLock.position.y + 64 - 20
	min_x = firstLock.position.x - margin
	for i in range(11):
		var lock = lockScene.instance()
		lock.position = firstLock.position + Vector2(76, 0) * (i + 1)
		locks.append(lock)
		$nailscontainer.add_child(lock)
		max_x = lock.position.x + margin
	var free_locks = locks.duplicate()
	for i in range(number_of_loose_locks):
		var unlock_index = randi() % free_locks.size()
		loose_locks.append(free_locks[unlock_index])
		free_locks.remove(unlock_index)
	
	for lock in loose_locks:
		lock.modulate = Color(0.8, 0.8, 1)
		# lock.position.y += 20
	for lock in free_locks:
		lock.nailed(true)
	playerspeed = initialSpeed
	hammer_sprite(hammer_hover)

func _process(delta):
	for nail in loose_locks:
		nail.lookAt($hammer.position)
	if status == State.INIT:
		if intro_over:
			status = State.AIMING
	elif status == State.AIMING:
		hammer_sprite(hammer_hover)
		player.position.y = floor_y
		player.position.x += playerspeed * delta * (1 + game_difficulty * 0.02)
		if (playerspeed < 0 and player.position.x < min_x) or (playerspeed > 0 and player.position.x > max_x):
			playerspeed *= -1
	elif status == State.HAMMERING:
		#player.position.y = secondRow
		timer_of_hammering += delta
		if timer_of_hammering >= hammer_duration:
			status = State.AIMING
	elif status == State.DONE:
		pass #play some animation depending on win/lose

var hammer_hover = 0
var hammer_hit = 1
var hammer_stun = 2

func hammer_sprite(index):
	if index == hammer_hover:
		$hammer/sprite_root/hover_sprite.visible = true
		$hammer/sprite_root/hit_sprite.visible = false
		$hammer/sprite_root/stun_sprite.visible = false
	elif index == hammer_hit:
		$hammer/sprite_root/hover_sprite.visible = false
		$hammer/sprite_root/hit_sprite.visible = true
		$hammer/sprite_root/stun_sprite.visible = false
	elif index == hammer_stun:
		$hammer/sprite_root/hover_sprite.visible = false
		$hammer/sprite_root/hit_sprite.visible = false
		$hammer/sprite_root/stun_sprite.visible = true

func on_game_timeout():
	$lose_sound.play()
	.on_game_timeout()

func _unhandled_input(event):
	if event.is_action_pressed("game_big_button_00"):
		if status == State.AIMING:
			timer_of_hammering = 0
			status = State.HAMMERING
			var the_bodies_hit_the_floor = $hammer/Area2D.get_overlapping_areas()
			var hit_anything = false
			for nail in the_bodies_hit_the_floor:
				if loose_locks.has(nail):
					hit_anything = true
					nail.nailed()
					nail.modulate = Color(1, 1, 1)
					loose_locks.erase(nail)
					if loose_locks.size() <= 0:
						emit_signal("game_finished", true, 0.3)
						$win_sound.play()
			if(hit_anything):
				for nail in loose_locks:
					nail.scareIt()
				$nail_hit_sound.stream.loop = false
				$nail_hit_sound.pitch_scale = 0.8 + 0.4 * randf() 
				$nail_hit_sound.play()
				hammer_sprite(hammer_hit)
			else:
				timer_of_hammering -= stun_duration
				$nail_fail_sound.stream.loop = false
				$nail_fail_sound.pitch_scale = 1 + 0.2 * randf() 
				$nail_fail_sound.play()
				hammer_sprite(hammer_stun)
	if event.is_action_released("game_big_button_00"):
		if status == State.HAMMERING:
			status = State.AIMING
