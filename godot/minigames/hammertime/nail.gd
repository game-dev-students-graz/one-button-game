extends Area2D

var anchor
var faceAnchor
var scaredy
var shook
var dead

func _ready():
	$normal.visible = true
	$hit.visible = false
	faceAnchor = $normal/Face.position
	anchor = position

func _process(delta):
	if scaredy:
		position = anchor + Vector2(rand_range(-3, 3), rand_range(-3, 3))
	else:
		position = anchor

func nailed(skipEffect = false):
	$normal.visible = false
	$hit.visible = true
	scaredy = false
	shook = false
	dead = true
	if skipEffect:
		_on_HitTimer_timeout()
	else:
		$HitTimer.start()

func scareIt():
	if !dead:
		shook = true
		$ScareTimer.start()

func lookAt(position):
	var delta = position - self.position
	var offset = 30
	if(abs(delta.x) < 60):
		offset = 5
		scaredy = true
		shook = false
		$normal/Face/NormalFace.visible = false
		$normal/Face/ScaredFace.visible = true
		$normal/Face/ShockedFace.visible = false
	elif(shook):
		offset = 55
		scaredy = false
		$normal/Face/NormalFace.visible = false
		$normal/Face/ScaredFace.visible = false
		$normal/Face/ShockedFace.visible = true
	else:
		scaredy = false
		$normal/Face/NormalFace.visible = true
		$normal/Face/ScaredFace.visible = false
		$normal/Face/ShockedFace.visible = false
	
	$normal/Face.position = faceAnchor + delta.normalized() * offset
	

func _on_HitTimer_timeout():
	$hit/FancyHit.visible = false


func _on_ScareTimer_timeout():
	shook = false
