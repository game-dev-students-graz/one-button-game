extends "res://BaseMinigame.gd"

var timer = 0.0

enum FishingState {
	NOT_BITING,
	BITING,
	RELEASED,
	NIBBLING,
	END
}

var currentState = FishingState.NOT_BITING
var events = []
# the min and max time in % of game time at which the fish will bite
export var nibbles = 0

export var min_relative_nibble_time = .1
export var max_relative_nibble_time = .6
export var min_relative_bite_time = .6
export var max_relative_bite_time = .8
# the time in seconds until the fish releases again
export var time_to_release = .75
export var time_to_release_nibble = .15
export var time_to_loss_after_release = 1
var rng = RandomNumberGenerator.new()

func _enter_tree():
	rng.randomize()
	nibbles = min(rng.randi_range(int(game_difficulty / 20), int(game_difficulty / 10)), 5)
	for i in range(nibbles):
		var nibble_interval = (max_relative_nibble_time - min_relative_nibble_time) / float(nibbles)
		var nibble_time = timer_duration * rng.randf_range(min_relative_nibble_time + (i * nibble_interval), min_relative_nibble_time + ((i + 1) * nibble_interval)) - 2 * time_to_release_nibble
		rng.randomize()
		events.push_back([nibble_time, FishingState.NIBBLING])
		events.push_back([nibble_time + time_to_release_nibble, FishingState.NOT_BITING])
	var min_time = 1
	if events.size() != 0:
		min_time = events.back()[0] + .3 
	var bite_time = rng.randf_range(min_time, timer_duration * max_relative_bite_time)
	events.push_back([bite_time, FishingState.BITING])
	events.push_back([bite_time + time_to_release, FishingState.RELEASED])
	#timer_duration = events.back()[0] + time_to_loss_after_release
	events.push_back([timer_duration + 5, FishingState.END])

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	timer += delta
	if timer >= events[0][0] and currentState != FishingState.END:
		currentState = events[0][1]
		if events[0][1] == FishingState.NIBBLING:
			$Fish.position = $fish_nibble_position.position
		if events[0][1] == FishingState.NOT_BITING:
			$Fish.position = $fish_start_position.position
		if events[0][1] == FishingState.BITING:
			$Fish.position = $Hook.position + $Hook/bite_position.position
			$Hook/BaitedHook.visible = false
			$Hook/HookBitten.visible = true
		if events[0][1] == FishingState.RELEASED:
			$Fish.position = $fish_start_position.position
			$Hook/HookBitten.visible = false
			$Hook/HookRope.visible = true
		events.pop_front()

func on_game_timeout():
	if currentState != FishingState.END:
		currentState = FishingState.END
		var finalFishPosition = $Fish.position + Vector2(1500, -100)
		var finalFishScale = $Fish.scale
		finalFishScale.x *= -1
		$Tween.interpolate_property($Fish, "scale", $Fish.scale, finalFishScale, 0.1, Tween.TRANS_LINEAR, Tween.EASE_OUT)
		$Tween.interpolate_property($Fish, "position", $Fish.position, finalFishPosition, 0.3, Tween.TRANS_QUAD, Tween.EASE_OUT)
		$Tween.start()
		.on_game_timeout()

func _unhandled_input(event):
	if event.is_action_pressed("game_big_button_00"):
		if intro_over and currentState != FishingState.END:
			if currentState == FishingState.BITING:
				var finalFishPosition = $Fish.position + Vector2(0, -700)
				$Tween.interpolate_property($Fish, "position", $Fish.position, finalFishPosition, 0.2, Tween.TRANS_QUAD, Tween.EASE_OUT)
				emit_signal("game_finished", true, 0.4)
			else:
				emit_signal("game_finished", false, 0.4)
			currentState = FishingState.END
			var finalHookPosition = $Hook.position + Vector2(0, -700)
			$Tween.interpolate_property($Hook, "position", $Hook.position, finalHookPosition, 0.2, Tween.TRANS_QUAD, Tween.EASE_OUT)
			$Tween.start()
