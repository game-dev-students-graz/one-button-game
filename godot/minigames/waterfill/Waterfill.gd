extends "res://BaseMinigame.gd"

onready var waterglass_sprite = $waterglass/waterglass
onready var waterglass_fill = $waterglass/waterglass/fill
onready var waterglass_lowerbound = $waterglass/waterglass/lower_bound
onready var waterglass_upperbound = $waterglass/waterglass/upper_bound
onready var tap_handle = $tap/handle

var fill_speed = 0.5 # 0.1 means it needs 10 seconds to fill the whole glass
var upperbound_y = 0.8 # height of the upper bound
var lowerbound_y = 0.7 # height of the lower bound
var bound_size_y = 0.02 # thickness of the upper and lower bound
var rotate_speed = 0.1 # 0.1 means it needs 10 seconds to fill the whole glass
var rotate_from
var rotate_to = -30
var rotate_percent = 0
var rotate_decrease_factor = 2

enum State {INIT, WATER_PAUSE, WATER_GO, DONE}

var status = State.INIT

func _enter_tree():
	# TODO: use the game difficulty!
	var game_difficulty = scaled_game_difficulty(0, 1)
	timer_duration = 5 - game_difficulty

func _ready():
	rotate_from = tap_handle.rotation_degrees
	
	# set fill amount
	waterglass_fill.rect_scale.y = 0.0
	# scale lower and upper bound
	waterglass_lowerbound.rect_scale.y = bound_size_y
	waterglass_upperbound.rect_scale.y = bound_size_y
	# position lower and upper bound
	var sprite_size = waterglass_sprite.get_texture().get_size()
#	print(sprite_size.x)
	waterglass_lowerbound.rect_position.y -= sprite_size.y * lowerbound_y
	waterglass_upperbound.rect_position.y -= sprite_size.y * upperbound_y
	# offset lower bound so that the player wins just when the water is BETWEEN (not touching) the bounds
	waterglass_lowerbound.rect_position.y += sprite_size.y * bound_size_y

func _process(delta):
	# set tap rotation
	tap_handle.rotation_degrees = lerp(rotate_from, rotate_to, rotate_percent)
	# set waterglass fill: use maximum fill speed and multiple it with how far the tap is opened
	waterglass_fill.rect_scale.y += fill_speed * rotate_percent * delta
	# winning condition: check if water stopped and inside bounds
	if status != State.DONE:
		if rotate_percent < 0.01 && lowerbound_y <= waterglass_fill.rect_scale.y && waterglass_fill.rect_scale.y <= upperbound_y:
			emit_signal("game_finished", true, 0.3)
			$win_sound.play()
			status = State.DONE
		elif waterglass_fill.rect_scale.y > upperbound_y:
			emit_signal("game_finished", false)
			$lose_sound.play()
			status = State.DONE
	if status == State.INIT:
		if intro_over:
			status = State.WATER_PAUSE
	elif status == State.WATER_PAUSE:
		# decrease tap rotation
		rotate_percent -= delta * rotate_decrease_factor
	elif status == State.WATER_GO:
		# increase tap rotation
		rotate_percent += delta
	elif status == State.DONE:
		pass #play some animation depending on win/lose
	# clamp tap rotation
	rotate_percent = clamp(rotate_percent, 0.0, 1.0)

func on_intro_over():
	if Input.is_action_pressed("game_big_button_00"):
		status = State.WATER_GO
	.on_intro_over()

func on_game_timeout():
	$lose_sound.play()
	.on_game_timeout()

func _unhandled_input(event):
	if event.is_action_pressed("game_big_button_00"):
		if status == State.WATER_PAUSE:
			status = State.WATER_GO
	elif event.is_action_released("game_big_button_00"):
		if status == State.WATER_GO:
			status = State.WATER_PAUSE
