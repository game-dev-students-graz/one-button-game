extends MeshInstance

export var growUp = true;
export var speedUp = 1.0;
export var minSize = .8;
export var maxSize = 1.2;

export var growFat = true;
export var speedFat = 1.0;
export var minWidth = .8;
export var maxWidth = 1.2;

export var rng_seed = 0;

var rng = RandomNumberGenerator.new()

# Called when the node enters the scene tree for the first time.
func _ready():
	rng.seed = rng_seed
	minSize = scale.y * minSize
	maxSize = scale.y * maxSize
	minWidth = scale.x * minWidth
	maxWidth = scale.x * maxWidth
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if growUp:
		scale.y += delta * speedUp
		if scale.y >= maxSize:
			growUp = false
			speedUp = rng.randf_range(.8, 1.2)
	else:
		scale.y -= delta * speedUp
		if scale.y <= minSize:
			growUp = true
			speedUp = rng.randf_range(.8, 1.2)
	if growFat:
		scale.x += delta * speedFat
		if scale.x >= maxWidth:
			growFat = false
			speedFat = rng.randf_range(.8, 1.2)
	else:
		scale.x -= delta * speedFat
		if scale.x <= minWidth:
			growFat = true
			speedFat = rng.randf_range(.8, 1.2)
	pass
