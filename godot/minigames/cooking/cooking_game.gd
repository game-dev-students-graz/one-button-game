extends  "res://BaseMinigame.gd"

enum CookingState {
	WON,
	LOST,
	RAW,
	DONE,
	BURNT
}

var timer = 0.0
export var ready_seconds = 2.0
export var burn_seconds = 3.0
export var loose_seconds = 3.5
var state = CookingState.RAW
var mat
var color_diif

export(Color) var raw = Color("a53f4c")
export(Color) var pre_done = Color("825412")
export(Color) var done = Color("53330a")
export(Color) var burnt = Color("1e1304")

# Called when the node enters the scene tree for the first time.
func _enter_tree():
	self.connect("game_finished", self, "game_finish_actions")
	mat = $meat.mesh.surface_get_material(1)
	mat.albedo_color = raw
	ready_seconds = rand_range(timer_duration * 0.3, timer_duration * 0.7)
	burn_seconds = ready_seconds + (0.4 + scaled_game_difficulty(0.7, 0))
	timer_duration = burn_seconds + 3

func game_finish_actions(did_win, outro_duration = 0):
	$Label.visible = false

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	timer += delta
	if state == CookingState.RAW:
		#TODO slowly change color
		changeColor()
	if timer >= ready_seconds:
		mat.albedo_color = done
		state = CookingState.DONE
	if timer >= burn_seconds:
		mat.albedo_color = burnt
		state = CookingState.BURNT

func _unhandled_input(event):
	if event.is_action_pressed("game_big_button_00"):
		if state == CookingState.DONE:
			emit_signal("game_finished", true)
		else:
			emit_signal("game_finished", false)
		
func changeColor():
	var new_color = mat.albedo_color
	new_color.r = raw.r + (timer / ready_seconds) * (pre_done.r - raw.r) 
	new_color.g = raw.g + (timer / ready_seconds) * (pre_done.g - raw.g) 
	new_color.b = raw.b + (timer / ready_seconds) * (pre_done.b - raw.b) 
	mat.albedo_color = new_color
