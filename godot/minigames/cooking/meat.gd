extends MeshInstance

var mat;
export var rotation_speed = 1.5

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	rotate_x(delta * rotation_speed);
	pass
