extends Node

var highscore_save_path = "user://one-button-game.highscores"
var list_of_games = load_minigames_from_file()
var highscores = load_highscores()


func load_highscores():
	var loadfile = File.new()
	if not loadfile.file_exists(highscore_save_path):
		return []
	loadfile.open(highscore_save_path, File.READ)
	var loadedScores = parse_json(loadfile.get_line())
	if loadedScores == null:
		loadedScores = []
	return loadedScores

func _notification(what):
	if what == MainLoop.NOTIFICATION_WM_QUIT_REQUEST:
		save_highscores()

func save_highscores():
	var savefile = File.new()
	savefile.open(highscore_save_path, File.WRITE)
	savefile.store_line(to_json(highscores))
	savefile.close()

func load_minigames_from_file(filename = "res://games.txt"):
	var result = []
	var f = File.new()
	f.open(filename, File.READ)
	while not f.eof_reached():
		var line = f.get_line()
		if !line.empty():
			result.append(line)
	f.close()
	return result

func get_current_list_of_games():
	return list_of_games

func set_current_list_of_games(list):
	list_of_games = list

func get_highscores():
	return highscores

func position_of_score(score):
	for i in range(len(highscores)):
		if highscores[i].score < score:
			return i
	return len(highscores)

func add_high_score(name, score):
	var position = position_of_score(score)
	highscores.insert(position, {"name":name, "score":score})
	return position

func top_scores_formatted(amount, highlight = -1):
	var text = ""
	var i = 0
	for entry in highscores:
		if i == highlight:
			text += "[wave]"
		text += entry.name
		if i == highlight:
			text += "[/wave]"
		text += "   ...   "
		text += str(entry.score)
		text += "\n"
		i += 1
		if i >= amount:
			break
	return text
