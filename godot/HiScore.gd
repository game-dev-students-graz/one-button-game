extends Control

var confirm_duration = 1
var input_period = 0.9
var confirm_timer = 0
var confirming = false

var letter_nr = 0
var letters = [0, 0, 0]
var alphabet = ['_', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']

func reset(score):
	confirming = false
	letters = [0, 0, 0]
	letter_nr = 0
	confirm_timer = 0
	$ScoreVBoxContainer/ScoreLabel.text = "Score: " + str(score)
	update_labels()
	update_marker()

func _process(delta):
	if confirming:
		confirm_timer += delta
		if confirm_timer > confirm_duration:
			confirm_timer = 0
			letter_nr += 1
			update_labels()
			confirming = false
	update_marker()

func _input(event):
	if not visible:
		return
	if event.is_action_pressed("game_big_button_00"):
		confirming = true
	elif event.is_action_released("game_big_button_00"):
		if confirming and confirm_timer < input_period:
			if letter_nr < len(letters):
				letters[letter_nr] += 1
				if letters[letter_nr] >= alphabet.size():
					letters[letter_nr] = 0
			update_labels()
		confirming = false
		confirm_timer = 0
	elif event is InputEventKey:
		if event.pressed and not is_done() and not event.is_echo():
			var letter = clamp(event.scancode - KEY_A + 1, 0, alphabet.size())
			letters[letter_nr] = letter
			letter_nr += 1
			update_labels()
			confirming = false

func is_done():
	return letter_nr >= 3

func get_entered_name():
	return alphabet[letters[0]] + alphabet[letters[1]] + alphabet[letters[2]]

func update_labels(): 
	$ScoreVBoxContainer/LetterHBoxContainer/Letter1.text = alphabet[letters[0]]
	$ScoreVBoxContainer/LetterHBoxContainer/Letter2.text = alphabet[letters[1]]
	$ScoreVBoxContainer/LetterHBoxContainer/Letter3.text = alphabet[letters[2]]

func update_marker():
	var p = confirm_timer / confirm_duration
	$MarkerSprite.scale = Vector2(1-p, 1-p)
	
	var offset = Vector2(20, 90)
	
	if letter_nr == 0:
		$MarkerSprite.position = $ScoreVBoxContainer/LetterHBoxContainer/Letter1.get_global_rect().position + offset
	elif letter_nr == 1:
		$MarkerSprite.position = $ScoreVBoxContainer/LetterHBoxContainer/Letter2.get_global_rect().position + offset
	elif letter_nr == 2:
		$MarkerSprite.position = $ScoreVBoxContainer/LetterHBoxContainer/Letter3.get_global_rect().position + offset
	elif letter_nr == 3:
		$MarkerSprite.scale = Vector2()

