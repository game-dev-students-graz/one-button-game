extends Node

# ######################################################################################################################
# Info
# 
# ++++++++++++++++++++++++++++++
# [Client] Initialization message:
# {
#   'type': ['JoyButton', 'JoyAxis'],
#   'index': 0
# }
#
# [Server] Response:
# 'initialized'
# 
# ++++++++++++++++++++++++++++++
# [Client] Input message JoyButton:
# 'p', 'r'
#
# [Server] No response
#
# ++++++++++++++++++++++++++++++
# [Client] Input message JoyAxis:
# Not implemented yet
#
# [Server] No response
#
# ++++++++++++++++++++++++++++++
# [Server] Latency test message:
# 'latencytest'
# 
# [Client] Response:
# 'latencytest'
#

# ######################################################################################################################
# Setup

var network_input_devices_	= null
var network_input_ui_		= null
var network_input_server_	= null

func _ready():
	self.network_input_devices_	= {}
	self.network_input_ui_		= self.get_node('UserInterface')
	self.network_input_server_	= NetworkInputServer.new(get_tree(), self.network_input_devices_, self.network_input_ui_)
	self.network_input_ui_.initInstances(self.network_input_devices_, self.network_input_server_)

func _process(delta):
	self.network_input_server_.poll()

# ######################################################################################################################
# NetworkInputDevice

class NetworkInputDevice:

	# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	# Members

	var websocket_id_			= null			# Id of own websocket
	var initialized_			= false			# [false, true]
	var type_					= null			# ['JoyButton', 'JoyAxis']
	var index_					= null			# 0
	var state_					= null			# ['r', 'p']
	var latency_measurements_	= []			# [2, 5, 10]; Last measurements in ms
	var latency_last_request_	= 0				# 0; Time the request was sent in ms

	# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	# Methods

	func _init(websocket_id):
		self.websocket_id_	= websocket_id

	func initialize(type, index, default_state):
		self.initialized_	= true
		self.type_			= type
		self.index_			= index
		self.state_			= default_state
	
	func uninitialize():
		self.initialized_	= false
		
	func isInitialized():
		return self.initialized_
		
	func getWebsocketId():
		return self.websocket_id_
		
	func getType():
		return self.type_
		
	func getIndex():
		return self.index_
	
	func getLatency():
		return self.latency_measurements_
	
	func getState():
		return self.state_
	
	func setState(state):
		self.state_ = state
	
	func latencyTestStart():
		self.latency_last_request_ = OS.get_ticks_msec()
		
	func latencyTestStop():
		var latency = OS.get_ticks_msec() - self.latency_last_request_
		self.latency_measurements_.push_back(latency)
		return latency

# ######################################################################################################################
# NetworkInputServer

class NetworkInputServer:

	# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	# Members

	# Settings
	var port_ = 8080

	# Instances
	var scene_tree_				= null
	var wss_ 					= null
	var network_input_devices_ 	= null
	var network_input_ui_		= null

	# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	# Methods

	func _init(scene_tree, network_input_devices, network_input_ui):
		# Instances
		self.scene_tree_			= scene_tree
		self.network_input_devices_	= network_input_devices
		self.network_input_ui_		= network_input_ui
		
		# Setup WebSocket server
		self.network_input_ui_.addLog('[info] Setting up WebSocket server')
		self.wss_ = WebSocketServer.new()
		self.wss_.connect('client_connected', self, '_client_connected')
		self.wss_.connect('data_received', self, '_data_received')
		self.wss_.connect('client_close_request', self, '_client_close_request')
		self.wss_.connect('client_disconnected', self, '_client_disconnected')
	
		# Start WebSocket server
		self.network_input_ui_.addLog('[info] Starting WebSocket server')
		if(self.wss_.listen(self.port_) == OK):
			self.network_input_ui_.addLog('[info] Listening on port ' + str(self.port_))
		else:
			self.network_input_ui_.addLog('[error] Could not start server')

	func poll():
		# Poll websocket
		if self.wss_.is_listening():
			self.wss_.poll()

	# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	# Methods: Websocket server events

	func _client_connected(websocket_id, protocol):
		self.network_input_ui_.addLog('[info] Client connected, websocket_id ' + str(websocket_id))
		self.network_input_devices_[websocket_id] = NetworkInputDevice.new(websocket_id)

	func _data_received(websocket_id):
		# self.network_input_ui_.addLog('[info] Data received, id ' + str(websocket_id))
		
		# Unpack data
		var payload = self.unpackData(websocket_id)
		
		if(!payload):
			return
		
		# Get input device
		var input_device = self.network_input_devices_[websocket_id]
		
		# Handle input message
		if(input_device.isInitialized()):
			
			# Handle latency test
			if(payload == 'latencytest'):
				var latency = input_device.latencyTestStop()
				self.network_input_ui_.addLog('[info] Device ' + str(websocket_id) + ' finished latency test. Latency: ' + str(latency))
				return
			
			# Handle JoyButton
			if(input_device.getType() == 'JoyButton'):
				self.handleJoyButton(input_device, payload)
				return
	
			# Handle JoyAxis
			if(input_device.getType() == 'JoyAxis'):
				self.handleJoyAxis(input_device, payload)
				return
			
			# Handle other
			self.network_input_ui_.addLog('[error] Device type is invalid. Data: ' + payload)
			return
		
		# Handle initialization messages
		if(!self.handleDeviceInitialization(input_device, payload)):
			return

	func _client_close_request(websocket_id, code, reason):
		self.network_input_ui_.addLog('[info] Client close request, id ' + str(websocket_id))
		self.network_input_devices_[websocket_id].uninitialize()

	func _client_disconnected(websocket_id, clean = true):
		self.network_input_ui_.addLog('[info] Client disconnected, id ' + str(websocket_id))
		self.network_input_devices_.erase(websocket_id)

	# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	# Methods: Handle websocket requested actions

	func unpackData(websocket_id):
		# Unpack packet
		var packet		= self.wss_.get_peer(websocket_id).get_packet()
		var is_string 	= self.wss_.get_peer(websocket_id).was_string_packet()
		
		# Check if it is a string packet
		if(!is_string):
			self.network_input_ui_.addLog('[error] Got non-string-packet')
			return false
			
		# Read payload
		return packet.get_string_from_utf8()

	func handleDeviceInitialization(input_device, payload):
		# Parse json
		var initialization_message = parse_json(payload)
				
		# If json invalid
		# TODO: Add default state
		if((typeof(initialization_message) != TYPE_DICTIONARY) ||
		   (!initialization_message.has_all(['type', 'index']))):
			self.network_input_ui_.addLog('[error] Initialization message json is invalid')
			return false
		
		# If message format invalid
		if(!(initialization_message['type'] == 'JoyButton' || initialization_message['type'] == 'JoyAxis') ||
		   typeof(initialization_message['index']) != TYPE_REAL):
			self.network_input_ui_.addLog('[error] Initialization message format is invalid')
			return false
			
		# Initialize device
		# TODO: Update default state for joyaxis
		input_device.initialize(initialization_message['type'], initialization_message['index'], 'r')

		# Send ack message to device
		self.wss_.get_peer(input_device.getWebsocketId()).put_packet('initialized'.to_utf8())
		self.network_input_ui_.addLog('[info] Device ' + str(input_device.getWebsocketId()) + ' initialized')
		return true

	func handleJoyButton(input_device, payload):
		if(payload == 'p'):
			self.add_input_event(input_device.getIndex(), true)
			input_device.setState('p')
				
		elif(payload == 'r'):
			self.add_input_event(input_device.getIndex(), false)
			input_device.setState('r')
				
		else:
			self.network_input_ui_.addLog('[error] Input message is invalid')

	func handleJoyAxis(input_device, payload):
		self.network_input_ui_.addLog('[error] Input type JoyAxis not implemented yet')

	func add_input_event(button_index = 0, pressed = true):
		# Create and add input event
		var input_event = InputEventJoypadButton.new()
		input_event.set_button_index(button_index)
		input_event.set_pressed(pressed)
		self.scene_tree_.input_event(input_event)

	# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	# Methods: Additional actions
	
	func testLatencyAll():
		for input_device in self.network_input_devices_.values():
			input_device.latencyTestStart()
			self.wss_.get_peer(input_device.getWebsocketId()).put_packet('latencytest'.to_utf8())
			self.network_input_ui_.addLog('[info] Device ' + str(input_device.getWebsocketId()) +
										  ' performing latency test')
