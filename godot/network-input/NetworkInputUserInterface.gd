extends Control

# ######################################################################################################################
# Members

# Instances
var network_input_server_ 		= null
var network_input_devices_		= null

# UI elements
var ui_network_log_text_		= null
var ui_network_devices_text_	= null

# ######################################################################################################################
# Engine methods

func _ready():
	# Link ui elements
	self.ui_network_log_text_		= self.get_node('NetworkInputLog/TextEdit')
	self.ui_network_devices_text_	= self.get_node('NetworkInputDevices/TextEdit')
	
	# Init
	self.visible = false

func initInstances(network_input_devices, network_input_server):
	self.network_input_devices_	= network_input_devices
	self.network_input_server_	= network_input_server

func _input(event):
	# Visibility
	if event.is_action_pressed('debug_network'):
		get_tree().set_input_as_handled()
		self.visible = !self.visible
	
	if event.is_action_pressed('debug_network_latency'):
		get_tree().set_input_as_handled()
		self.network_input_server_.testLatencyAll()

# ######################################################################################################################
# Input Overview
func _process(delta):
	if(!self.visible):
		return
	
	var textfield = self.ui_network_devices_text_
	textfield.select_all()
	textfield.cut()
	
	for input_device in self.network_input_devices_.values():
		textfield.insert_text_at_cursor('[' + str(input_device.getWebsocketId()) + ']\n')
		textfield.insert_text_at_cursor('> Initialized	: ' + str(input_device.isInitialized()) + '\n')
		textfield.insert_text_at_cursor('> Type       	: ' + str(input_device.getType()) + '\n')
		textfield.insert_text_at_cursor('> Index      	: ' + str(input_device.getIndex()) + '\n')
		textfield.insert_text_at_cursor('> State      	: ' + str(input_device.getState()) + '\n')
		textfield.insert_text_at_cursor('> Latency    	: ' + str(input_device.getLatency()) + '\n')

# ######################################################################################################################
# Network log

func addLog(log_text):
	print(log_text)
	self.ui_network_log_text_.insert_text_at_cursor(log_text)
	self.ui_network_log_text_.insert_text_at_cursor('\n')
	
