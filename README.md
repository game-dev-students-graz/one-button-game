# one-button-game
by Gamedev Students Graz

## General infos
* Use Godot Engine v3.3.x
* Use GLES2 Renderer

## How to create a Minigame

### Setup Main Scene
First create a folder `godot/minigames/<name>` for your minigame. This folder is where you should put all your scenes, scripts and assets needed for your minigame.

Then create a New Scene, with a `Node2D` as the root node ("2D Scene"), which is the main scene of your minigame. Attach a script to the root node of your scene with the following content:

```
extends "res://BaseMinigame.gd"
```

This should add a few Script Variables to the Properties of the node in the Inspector to the right.

* _Minigame Name_: might be displayed to the player eventually
* _Timer Duration_: The time (in seconds) the player has to complete the minigame.
* _Intro Duration_: The time (in seconds) from showing the game to actually starting the timer.
* _Timeout Win_: Whether a timeout should result in a loss (false) or a win (true).
* _Outro Duration_: The time (in seconds) from ending the game, to starting the transition to the next minigame.

If you need to change the value of `timer_duration` or `intro_duration` dynamically, you can set them in the `_enter_tree` function, like this:

```
func _enter_tree():
    timer_duration = rand_range(2, 5)
```

### Signals and Properties
To indicate that your game has been won or lost, the signal `game_finished` has to be emitted from the root node, with a boolean parameter indicating success.
Optional: The second parameter indicates the number of seconds to wait for the transition. Defaults to 0 seconds.

e.g. When the minigame is won:
```
emit_signal("game_finished", true, 2)
```

To know if the intro duration for your minigame has elapsed, the `intro_over` property of the root node of your minigame can be queried.

The functin `on_game_timeout` will be called once the time for the game is over. This can be overridden to do additional actions when the timer is over:

```
func on_game_timeout():
    .on_game_timeout() //call parent function
    //do something else e.g. play some animation and sound effect
```

#### Difficulty

Higher difficulty modifiers should add challenge by introducing slight twists or adding additional components.

To query the current difficulty modifier use `game_difficulty`.
Theis property is set before the minigame is added to the scene, therefore it is available inside `_enter_tree` and `_ready`.

The `game_difficulty` modifier of the game is given as a value from 1 to 100.

### Adding the Minigame to the pool of Minigames

Add your game to the `listOfGames` inside the `MainGame.gd` script, to add it to the pool of available minigames.

## Minigames 
### Game 1
