from uwebsockets import client
from machine import Pin
import time

# Endless loop
while True:

	####################################################################################################################
	# Initialization

	# Init values
	g_pin_led		= Pin(02, Pin.OUT)
	g_pin_button	= Pin(14, Pin.IN, Pin.PULL_UP)
	g_state_last	= 0
	g_state_new		= 0
	g_initialized	= False

	# Turn on led
	g_pin_led.value(0);

	# Wait for wifi to connect
	time.sleep(3)

	# Connect to websocket
	try:
		ws = client.connect('ws://192.168.1.2:8080/')

		# Initialize as JoyButton
		ws.send('{\"type\":\"JoyButton\",\"index\":0}')

		#if b'initialized' == ws.recv():
		g_initialized = True
		g_pin_led.value(1)

		
		if b'latencytest' == ws.recv():
			ws.send('latencytest')

	except OSError:
			g_pin_led.value(1)
			time.sleep(1)
			g_pin_led.value(0)
	

	####################################################################################################################
	# Input handling

	while g_initialized:

		try:
			g_state_new = 1 - g_pin_button.value()

			if g_state_last == 0 and g_state_new == 1:
				ws.send('p')
				g_state_last = 1
				g_pin_led.value(0)

			elif g_state_last == 1 and g_state_new == 0:
				ws.send('r')
				g_state_last = 0
				g_pin_led.value(1)

		except OSError:
			g_initialized = False

